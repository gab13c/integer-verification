#Script needs to read verilog file (generate for now)
#take the file and strash -> creates an aiger
#take the aiger and replace all numbers pass the first like with letters

#write a procedure that takes in 2 inputs (need some way to tell what type of gates those inputs are)

#list: Letters for primary inputs
#dict for internal edges: key: name, value: gate type
#dict for primary outputs: key: name, value: gate type


#TCL Script below:

proc sortGateDictonary {circuitGates} {
}

proc numberOfInputs {firstLine} {
        set firstLineList [split $firstLine " "]
        return [lindex $firstLineList 2]
}


proc numberOfOutputs {firstLine} {
        set firstLineList [split $firstLine " "]
        return [lindex $firstLineList 4]
}

proc extractInputList {contents inputSize} {
        return [lrange $contents 1 $inputSize]
}

proc extractOutputList {contents inputSize outputSize} {
	return [lrange $contents [expr $inputSize + 1] [expr $inputSize + $outputSize]]
}

proc createDictOfGates{contents startLine} {
        set sublist [lrange $contents $startLine]
        puts $sublist
        dict create gates
        foreach item $sublist {
                set currentLineGate [split $item " "]
                set typeOfGate [lindex currentLineGate 1] [lindex currentLineGate 2]  
                dict set gates [lindex currentLineGate 0] typeOfGate
        }
        return $gates
}

# a and b are 2 inputs c is output
proc typeOfGate {c a b} {
        puts "a is: $a"
        puts "b is : $b"

        set a_mod [expr $a % 2]
puts "a % 2 = $a_mod"
set b_mod [expr $b % 2]
puts "b % 2 = $b_mod"
set c_mod [expr $c % 2]
puts "c % 2 = $c_mod"

        if {$a_mod == 0 && $b_mod == 0} then {
       set answer "and , inputs: $a , $b"
        } elseif {$a_mod == 0 && $b_mod == 1} then {
                set answer "and , inputs: $a , $b"
        }  elseif {$a_mod == 1 && $b_mod == 0} then {
                set answer "and , inputs: $a , $b"
         }  else {
                set answer "nor , inputs: $a , $b"
         }
puts "$answer \n"
return $answer
}



proc createGates {contents start} {
        set sublist [lrange $contents $start  [llength $contents]]
puts "Sublist of gates is: "
        puts "$sublist \n"
        foreach item $sublist {
                set currentLineGate [split $item " "]
                set gateType [typeOfGate [lindex $currentLineGate 0] [lindex $currentLineGate 1] [lindex $currentLineGate 2]]
#check to see if output is inverted
set output_mod [expr [lindex $currentLineGate 0] % 2]
if {$output_mod == 1} {
dict set gates [expr [lindex $currentLineGate 0] -1] $gateType
} else {
                       dict set gates [lindex $currentLineGate 0 ]  $gateType
}
        }
        return $gates
}

proc sortDictFromPrimaryInputToPrimaryOutput {dictGates inputList outputList} {
	#once we are done iterating through whole list we will put done == 1
	set done 0
	#start iteration after the primary inputs since they arent gates
	set startLine [llength $inputList]

	#currentLevelInputs is the inputs to the current level, based on the inputs to the level we know what level we are on
	set currentLevelInputs $inputList
	set nextLevelInputs {"Next" "Level" "Inputs" "Are: "}
while {$done == 0} {
	#need to reset nextLevelInputs for each iteration so done at start
       puts "Reseting Next Level Inputs"
       puts "Current Next Level Inputs length is: [llength $nextLevelInputs]"
       if {[llength $nextLevelInputs] > 1} {
       while {1 < [llength $nextLevelInputs]} {
       	       puts "Replacing [lindex $nextLevelInputs 1]"
               set nextLevelInputs [lreplace $nextLevelInputs 1 1]
               puts "Current Next Level Inputs length is: [llength $nextLevelInputs]"
               puts "Next level inputs are: $nextLevelInputs"
        	 }      
	}    
	puts "\n"
	foreach item [dict keys $dictGates] {
		foreach output $outputList {
			if {$item != $output} {
				set skip 0
				continue
			} else {
				set skip 1
				break
			}
		}
		if {$skip == 1} {
			continue
		}

        	set value [dict get $dictGates $item]
		puts "\nCurrent Line is $value"
		set currentLine [split $value " "]
		foreach inputCurrLevel $currentLevelInputs {
		puts "Currently Comparting Input: $inputCurrLevel to 3: [lindex $currentLine 3] and 5: [lindex $currentLine 5]"
		# i decided to keep inputs as even and odd(so we know when input is inverted) so you must consider both cases when its inverted and when its not for the levels
		if {[expr {[lindex $currentLine 3] == $inputCurrLevel || [expr [lindex $currentLine 3] - 1] == $inputCurrLevel}] || [expr {[lindex $currentLine 5] == $inputCurrLevel || [expr [lindex $currentLine 5] - 1] == $inputCurrLevel}]  } then {
			puts "Adding $item -> $value to sortedGates Dict \n"
			dict set sortedGates $item $value
			lappend nextLevelInputs $item
			break
			}
		}
  }
  if {[llength $nextLevelInputs] < 2} {
          set done 1
                   puts "Done with Sorting breaking out of while loop"
           }
           puts "Setting Next Level Inputs to Curr Level Inputs \n"                
           set currentLevelInputs $nextLevelInputs
           puts "Curr Level Inputs Are $currentLevelInputs \n"
	}

	set index 0
	foreach item $outputList {
		set value [dict get $dictGates $item]
		dict set sortedGates $item $value 
		set index [expr $index + 1]
	}
	return $sortedGates
	}

#since each output does not know its inverted, when its input to the next level its either inverted or not, we will need to reclassify
#each output on whether or not it is inverted on the next level, if it is it will become a different gate nor -> or, and -> nand
proc reclassifyEachGate {dictGates} {
#will do if needed later, worried about fanout, will have to track which fanout it is and such
}

#we will be iterating over each key value in the sorted dict and determining if there are any xors
#afte determining the xors we will replace the gate value in lindex 0 of the value for the key
proc determineXORs {dictGates} {
	foreach item [dict keys $dictGates] {
		set value [dict get $dictGates $item]
		if {$value != "pi"} {
		#split each line, grab the 2 inputs and grab there gate from the values property
		set currentLine [split $value " "]
		puts "\nCurrent Line is: $currentLine"
		set currentInput1 [lindex $currentLine 3]
		#need to check if input is inverted because keys are even
		if {$currentInput1 == "pi"} {
			#cant do mod operation on a string
		} else {
			if {[expr $currentInput1 % 2] == 1} {
			set input1 [expr $currentInput1 -1]
		} else {
			set input1 $currentInput1
		}
	}
	puts "Input 1 is: $currentInput1"
	set currentInput1Value [split [dict get $dictGates $input1] " "]
	set typeOfGate1 [lindex $currentInput1Value 0]
	puts "Type of Gate Input 1 is: $typeOfGate1\n"
	set currentInput2 [lindex $currentLine 5]
               if {$currentInput2 == "pi"} {
                                #cant do mod operation on a string
                        } else {
                                if {[expr $currentInput2 % 2] == 1} {
                                        set input2 [expr $currentInput2 -1]
                                } else {
					set input2 $currentInput2
				}
                        }

		puts "Current Input 2 is: $currentInput2"
		set currentInput2Value [split [dict get $dictGates $input2] " "]
		set typeOfGate2 [lindex $currentInput2Value 0]
		puts "Type of Gate Input 2 is: $typeOfGate2\n"
		if {$typeOfGate1 == "pi" || $typeOfGate2 == "pi"} then {
		puts "Current Gate is based off Primary Inputs"
		} else {	
			puts "Input1 is: $input1 and CurrentInput1 is: $currentInput1"
			puts "Input2 is: $input2 and CurrentInput2 is: $currentInput2"
			puts "TypeOfGate1 is: $typeOfGate1  and TypeOfGate2 is: $typeOfGate2"
		if {[expr {$typeOfGate1 == "nor" && $typeOfGate2 == "and"}]} then {
		#we need to check if the nor and and are inverted to become or and nand
		#to do so i am going to check to see if the input1 is different from currentInput1
		#if it is that means the input was inverted, if not then its not inverted
			puts "Entered Loop 1"
			if { $input1 != $currentInput1} {
				puts "Changing Gate 1 to or"
				set typeOfGate1 "or"
			}		
			if { $input2 != $currentInput2} {
				puts "Changing gate 2 to nand"
				set typeOfGate2 "nand"
			}
puts "Changed gates are now TypeOfGate1 is: $typeOfGate1  and TypeOfGate2 is: $typeOfGate2"
if { $typeOfGate1 == "or" && $typeOfGate2 == "nand"} {
puts "\n$item is a XOR gate"
set dictGates [dict replace $dictGates $item "xor , inputs: $currentInput1 , $currentInput2"]
puts "Xor Dict is now: "
puts $dictGates
}
} elseif {[expr {$typeOfGate1 == "and" && $typeOfGate2 == "nor"}]} then {
puts "Entered Loop 2"
                                if { $input1 != $currentInput1} {
puts "Changing Type of Gate 1 to : nand \n"
                                        set typeOfGate1 "nand"
puts "Type of Gate 1 is now: $typeOfGate1"
                                }  
                                if { $input2 != $currentInput2} {
puts "Changing Type of Gate 2 to : or \n"
                                        set typeOfGate2 "or"
puts "Type of Gate 2 is now: $typeOfGate2"
                                }  
puts "Changed gates are now TypeOfGate1 is: $typeOfGate1  and TypeOfGate2 is: $typeOfGate2"
                                if { $typeOfGate1 == "nand" && $typeOfGate2 == "or"} {
                                        puts "\n$item is a XOR gate"
                                        set dictGates [dict replace $dictGates $item "xor , inputs: $currentInput1 , $currentInput2"]
puts "Xor Dict is now: "
                                        puts $dictGates

                        }

}
   }
}
}
set newXorDict $dictGates
return $newXorDict
}

proc listEquality {list1 list2} {
    foreach elem $list1 {
        if {$elem ni $list2} {return false}
    }
        foreach elem $list2 {
            if {$elem ni $list1} {return false}
        }
        return true
    }
   
#XOR is a 3 level AIG so we need to grab the primary inputs to compare them to other gates for sum and carry purposes
#since the NOR inputs are inverted to the XOR in the graph we will not use those but use the AND inputs
proc primaryInputsOfXORGate {dictGates andGate} {
   
    set inputs1 [split [dict get $dictGates $andGate] " "]
    puts "Input to the and gate for the AIG is: "
    set inputsToXORGate [list [lindex $inputs1 3] [lindex $inputs1 5]]
    return $inputsToXORGate
}

#need to figure out which gate is the NOR and AND for XOR AIG structure

#searching for xor and ands sharing the same inputs
proc determineAsscoiatedSumAndCarryGates {dictGates} {
    set associatedSumAndCarryGates {}
    dict for {typeOfOutput1 typeOfInputs1} $dictGates {
        set typeOfGate1 [lindex [split $typeOfInputs1 " "] 0]
        if {$typeOfGate1 == "xor"} {
       
       
        set inputs1 [lindex [split $typeOfInputs1 " "]]
        puts "Inputs 1 before parsing is: "
        puts $inputs1
        set inputsToGate1 [list [lindex $inputs1 3] [lindex $inputs1 5]]
        puts "Inputs to Gate 1 is :"
        foreach item $inputsToGate1 {
            puts $item
            #so we need to grab inputs of the inputs for this part (XOR is 3 levels)
            set initialInputs [dict get $dictGates [expr $item -1]]
            set typeOfInitialGate [ lindex [split $initialInputs " "] 0]
            puts "Type Of Initial Gate for XOR AIG is: "
            puts $typeOfInitialGate
            if {$typeOfInitialGate == "and"} {
                set inputsToGate1 [primaryInputsOfXORGate $dictGates [expr $item -1]]
            }
        }
       
        set inputsToGate1 [lsort -increasing $inputsToGate1]
        dict for {typeOfOutput2 typeOfInputs2} $dictGates {
            set typeOfGate2 [lindex [split $typeOfInputs2 " "] 0]
            if {$typeOfGate1 == $typeOfGate2} {
                continue
            } elseif {$typeOfGate2 == "and"} {
                set inputs2 [lindex [split $typeOfInputs2 " "]]
                puts "Inputs 1 before parsing is: "
                puts $inputs2
                set inputsToGate2 [list [lindex $inputs2 3] [lindex $inputs2 5]]
                        puts "Inputs to Gate 1 is :"
                    foreach item $inputsToGate2 {
                        puts $item
                    }
                if {[listEquality $inputsToGate1 $inputsToGate2] == true} {
                      puts "The outputs $typeOfOutput1 , $typeOfOutput2 are associated sum and carrys"
                      lappend associatedSumAndCarryGates " $typeOfOutput1 , $typeOfOutput2 "
                }
            }
        }
    }
  }
  return $associatedSumAndCarryGates
}

proc renameNumbersToVariables {dictGates inputSize outputList} {
    set mapping [dict create ]
    set iteration 0
    set internal "i_$iteration"
    set outputIter 0
    set primaryIndex_1 0 
    set primaryIndex_2 0

            foreach item $outputList {
                        set output "z_$outputIter"
                        puts "Mapping $item to $output"
                        dict append mapping $item $output
                        set outputIter [expr $outputIter + 1]
       	    }   

    puts "Input size is [expr $inputSize / 2]"	
    dict for {theKey theValue} $dictGates {
	        foreach {item} $outputList {
                if {$item == $theKey} {
                        set skip 1
                        break
                } else {
                        set skip 0
                        continue
                }
	}   

	 if { $skip == 0 } {
		if {$theValue == "pi"} {
			if {$primaryIndex_1 < [expr $inputSize / 2]} {
				set primaryInput_1 "a_$primaryIndex_1"
				puts "Mapping $theKey to $primaryInput_1"
				dict append mapping $theKey $primaryInput_1
				set primaryIndex_1 [expr $primaryIndex_1 + 1]
			} else {
				set primaryInput_2 "b_$primaryIndex_2"
				puts "Mapping $theKey to $primaryInput_2"
        	                dict append mapping $theKey $primaryInput_2
                	        set primaryIndex_2 [expr $primaryIndex_2 + 1]
			}	 
        } else {
        	puts "Mapping $theKey to $internal"
        	dict append mapping $theKey $internal 
        	set iteration [expr $iteration + 1]
		set internal "i_$iteration"
	
	}
	}		
    }
    return $mapping
}


proc renameDictToVariables {dictGates associatedPairList inputSize outputList vanishingPolynomialList} {
    #link variable in stack frame
    upvar 1 $vanishingPolynomialList currentVanishList
    set currentVanishList {}
    set mapping [renameNumbersToVariables $dictGates $inputSize $outputList]
    puts "Mapping Dict is: "
    dict for {theKey theValue} $mapping {
    puts "$theKey -> $theValue"
    }
    set relabeledXORDict [dict create ]
    dict for {theKey theValue} $dictGates {
        set inverted_1 0
        set inverted_2 0
        if {$theValue != "pi"} {
          set newKey [dict get $mapping $theKey]
                set value [split $theValue " "]
                set value_1 [lindex $value 3]
                if {[expr $value_1 % 2] != 0} {
                    set value_1 [expr $value_1 -1]
                    set inverted_1 1
                }
                set value_2 [lindex $value 5]
                if {[expr $value_2 % 2] != 0} {
                    set value_2 [expr $value_2 -1]
                    set inverted_2 1
                }
                set newInput1 [dict get $mapping $value_1]
                puts "New Input 1 is $newInput1"
                set newInput2 [dict get $mapping $value_2]
                puts "New Input 2 is $newInput2"
               
                               
                 if {$inverted_1 == 0 && $inverted_2 == 0} then {
                    set newValue "[lindex $value 0] , inputs: $newInput1 , $newInput2"
                    puts "New Value is $newValue"
                    dict append relabeledXORDict $newKey $newValue
                } elseif {$inverted_1 == 0 && $inverted_2 ==1} then {
                    set newValue "[lindex $value 0] , inputs: $newInput1 , !$newInput2"
                    puts "New Value is $newValue"
                    dict append relabeledXORDict $newKey $newValue
                } elseif {$inverted_1 == 1  && $inverted_2 ==0} then {
                    set newValue "[lindex $value 0] , inputs: !$newInput1 , $newInput2"
                    puts "New Value is $newValue"
                    dict append relabeledXORDict $newKey $newValue
                } else {
                    set newValue "[lindex $value 0] , inputs: !$newInput1 , !$newInput2"
                    puts "New Value is $newValue"
                    dict append relabeledXORDict $newKey $newValue            
                } } else {
            dict append relabeledXORDict [dict get $mapping $theKey] $theValue
        }
    }

#relabeling the elements in the associated list
    if {[llength $associatedPairList] != 0} {

        puts "Associated Pair List is: "
        puts $associatedPairList

          foreach item $associatedPairList {
            set splitAssociatedPair [split $item " "]
             dict get $mapping [lindex $splitAssociatedPair 1]
             set mappedAssociatedPairList " "
             set mappedInput1 [dict get $mapping [lindex $splitAssociatedPair 1]]
             set mappedInput2 [dict get $mapping [lindex $splitAssociatedPair 3]]
             lappend currentVanishList "{ $mappedInput1 , $mappedInput2 }"
           }
    }
    return $relabeledXORDict
}



proc writePolynomials {dictGates vanishingPolynomialList} {
    set polynomials {}
    dict for {theKey theValue} $dictGates {
        if {$theValue != "pi"} {
            set value [split $theValue " "]
            set typeOfGate [lindex $value 0]
   set value_1 [lindex $value 3]
            set value_2 [lindex $value 5]
            if {$typeOfGate == "and"} {
                lappend polynomials "-1*$theKey + $value_1 * $value_2"
            } elseif {$typeOfGate == "xor"} {
    regsub -all {[!]} $value_1 {} value1
    regsub -all {[!]} $value_2 {} value2
    set value_1 "($value1 + 1)"
    set value_2 "($value2 + 1)"
    lappend polynomials "-1*$theKey + $value_1 + $value_2"
     }
        }
    }

    foreach item $vanishingPolynomialList {
        set value [split $item " "]
        set value1 [lindex $value 1]
        set value2 [lindex $value 3]
        lappend polynomials "$value1 * $value2"
    }

    return $polynomials
}

proc power {base p} {
    set result 1
    while {$p>0} {
        set result [expr $result*$base]
        set p [expr $p-1]
    }
    return $result
}


proc createRingForSingular { fp relabeledXORDict polynomials inputList outputList} {
    #get field size
    global fieldSize

  
    #initial ring declaration
    set ring "ring r1 = 0, (Z,A,B, "
    set ringParameterSize 0
    set ringList {}
    puts "\nRelabeled XOR dict is: "
    dict for {theKey theValue} $relabeledXORDict {
    	append ringList " " $theKey
    }

    puts $ringList 
    set listLength [expr [llength $ringList]]
    #set ringList [lreplace $ringList $listLength $listLength] 
    puts "Ring List is $ringList"


    set splitList [split $ringList " "]
    puts "Split List is $splitList"

    for {set size [expr [llength $splitList]-1]} {$size >= 0} {set size [expr $size - 1]} {
		if {$size >1} {
			append ring "[lindex $splitList $size], "
		} else {
			append ring "[lindex $splitList $size]), rp;\n"
			break
		}
	}

    puts $fp "$ring"

	#create word level polynomials for inputs and ouputs
	puts "Creating Input Word Level Polynomials: "
	set iteration 0
	set inputSize [expr [llength $inputList] / 2]
	puts "Input Size is $inputSize"
	set startingScalar [expr 2 ** [expr $inputSize -1]]
	puts "Starting Scalar is: $startingScalar"
	set inputScalar 1
	set input1Poly "poly input_1 = -1*A + "
	set index [expr $inputSize - 1]
	puts "Starting Index is $index"
	for {set iteration $inputSize} {$iteration > 0} {set iteration [expr $iteration -1]} {
		puts "Starting input 1 for loop"
		puts "iteration is currently $iteration"
		if {$iteration > 1 } {
			append input1Poly "$startingScalar * a_$index + "
			set startingScalar [expr $startingScalar / 2]
			set index [expr $index -1]
		} else {
			append input1Poly "$startingScalar * a_$index;"
		}
	}
		puts "Input1Poly is $input1Poly"
		append ring "$input1Poly\n"
		puts $fp "$input1Poly\n"

        set inputScalar 1
        set input2Poly "poly input_2  = -1*B + "
	set startingScalar [expr 2 ** [expr $inputSize -1]]
	set index [expr $inputSize - 1]
        for {set iteration $inputSize} {$iteration > 0} {set iteration [expr $iteration -1]} {
                puts "Starting input 1 for loop"
                puts "iteration is currently $iteration"
                if {$iteration > 1 } {
                        append input2Poly "$startingScalar * b_$index + "
                        set startingScalar [expr $startingScalar / 2]
                        set index [expr $index -1]
                } else {
                        append input2Poly "$startingScalar * b_$index;"
                }
        }
        puts "Input2Poly is $input2Poly"
	append ring "$input2Poly\n"
	puts $fp "$input2Poly\n"
	

	#now doing ouput scalar
	set outputSize [expr [llength $outputList] - 1]
	set startingScalar [ expr 2 ** $outputSize]
	set index [expr $outputSize]
	set outputPoly "poly output = -1*Z + "
        for {set iteration [expr $outputSize + 1]} {$iteration > 0} {set iteration [expr $iteration -1]} {
                puts "Starting input 1 for loop"
                puts "iteration is currently $iteration"
                if {$iteration > 1 } {
                        append outputPoly "$startingScalar * z_$index + "
                        set startingScalar [expr $startingScalar / 2]
                        set index [expr $index -1]
                } else {
                        append outputPoly "$startingScalar * z_$index;"
                }
        }
        puts "OutputPoly is $outputPoly"
	append ring "$outputPoly\n"
	puts $fp "$outputPoly\n"
 
   
    set iteration 0
    set alphabet [split "a_$iteration b_$iteration c_$iteration d_$iteration e_$iteration f_$iteration g_$iteration h_$iteration j_$iteration k_$iteration l_$iteration m_$iteration n_$iteration o_$iteration p_$iteration q_$iteration r_$iteration s_$iteration t_$iteration u_$iteration v_$iteration w_$iteration" " "]
    set index 0
    #add polynomials to sing file
    foreach poly $polynomials {
        if {$index == [expr [llength $alphabet] -1] } {
        set iteration [expr $iteration + 1]
        set alphabet [split "a_$iteration b_$iteration c_$iteration d_$iteration e_$iteration f_$iteration g_$iteration h_$iteration j_$iteration k_$iteration l_$iteration m_$iteration n_$iteration o_$iteration p_$iteration q_$iteration r_$iteration s_$iteration t_$iteration u_$iteration v_$iteration w_$iteration" " "]
        }
        append ring "poly [lindex $alphabet $index] = $poly;\n"
        puts $fp "poly [lindex $alphabet $index] = $poly;\n"
        set index [expr $index + 1]
    }
    puts $ring
   
    #add vanishing polynomials
    foreach item [dict keys $relabeledXORDict] {
        if {$index == [expr [llength $alphabet]] } {
            set iteration [expr $iteration + 1]
            set alphabet [split "a_$iteration b_$iteration c_$iteration d_$iteration e_$iteration f_$iteration g_$iteration h_$iteration i_$iteration j_$iteration k_$iteration l_$iteration m_$iteration n_$iteration o_$iteration p_$iteration q_$iteration r_$iteration s_$iteration t_$iteration u_$iteration v_$iteration w_$iteration x_$iteration y_$iteration z_$iteration" " "]
            set index 0
        }
        append ring "poly [lindex $alphabet $index] = $item^2 - $item;\n"
        puts $fp "poly [lindex $alphabet $index] = $item^2 - $item;\n"
        set index [expr $index + 1]
    }
   
    puts $ring

    set makeIdeal [split $ring "\n"]
    set index  1
    set ideal "ideal J =  "
    foreach item $makeIdeal {
	if {[regexp {[A-Z,a-z]} $item]} {
		
	if {$index == 1} {
		set index [expr $index + 1]
		continue
	}
	if { $index < [expr [llength $makeIdeal] -1]} {
		set currentPoly [split $item " "]
		append ideal "[lindex $currentPoly 1], "
		puts $ideal
	} else {
		set currentPoly [split $item " "]
                append ideal "[lindex $currentPoly 1];"
                puts $ideal 
		}
	}
	set index [expr $index + 1]
    }
	append ring $ideal
	puts $fp $ideal

    puts $ring
   
}


proc removeLinesFromEndOfFile {lines} {
    puts "Starting remove line procedure"
    set currIndex 0
    set newContent $lines
    foreach line $lines {
    puts "Current Line is: $line"
    set currLine [split $line ""]
    set firstItem [lindex $currLine 0]
    puts "First Item is $firstItem"
    if {[regexp {[A-Z,a-z]} $firstItem] || $firstItem eq ""} then {
    set newContent [lreplace $newContent $currIndex $currIndex]
    puts "New Content is: $newContent"
    } else {
    set currIndex [expr $currIndex +1]
        }
    }
    return $newContent
}


    puts "Generating the multiplier \n"
    #generate command
   
    puts "Structural hasing the AIG \n"
    #strash
   
    set filename test.aag
   
    puts "Writing file to $filename \n"
    #	write_aiger $filename
    #	ly is $input1Poly
   
    puts "Opening $filename \n"
    set fp [open $filename r]
   
    puts "Setting each file line to a member of a list in sequential order \n"
    set contents [read $fp]
    puts $contents
    close $fp

set fieldSize 4
#set contents "aag 14 4 0 4 10\n2\n4\n6\n8\n10\n20\n28\n24\n10 6 2\n12 6 4\n14 8 2\n16 14 12\n18 15 13\n20 19 17\n30 14 12\n22 8 4\n24 22 16\n26 23 17\n28 27 25"
puts "Splitting the lines of the list"
set lines [split $contents "\n"]
puts "$lines \n"

puts "Removing Lines from end of file that arent associated with a gate"
set updatedLines [removeLinesFromEndOfFile $lines]

puts "Determining input size"
set inputSize [numberOfInputs [lindex $lines 0]]
puts "$inputSize \n"

puts "Determining output size"
set outputSize [numberOfOutputs [lindex $lines 0]]
puts "$outputSize \n"

puts "Determining Input List"
set inputList [extractInputList $lines $inputSize]
puts "$inputList \n"

#start at line $inputSize and go to line $ouputSize
puts "Determining Output List: "
set outputList [extractOutputList $lines $inputSize $outputSize]
puts "$outputList \n"

set outputList [lsort -integer $outputList]
puts "Sorted OutputList is : $outputList"

set start [expr $inputSize + $outputSize]


#need to create word level polynomial for output


#pi stands for primary input
puts "Adding primary inputs to list of gates for XOR determination"
foreach {input} $inputList {
    dict set inputDict $input "pi"
}        

puts "\nCreating Circuit Gates Dictonary"
set internalCircuitGates [createGates $updatedLines $start]

puts "\nMerging Input Dict to internalCircuitGatesDict"
set circuitGates [dict merge $inputDict $internalCircuitGates]

puts "output value of gate, and inputs that gate from aiger are: "
dict for {theKey theValue} $circuitGates {
    puts "$theKey -> $theValue"
}

puts "\nSorting the gates from primary input to primary output"
set sortedDictGates [sortDictFromPrimaryInputToPrimaryOutput $circuitGates $inputList $outputList]

set sortedDictGatesFinal [dict merge $inputDict $sortedDictGates]
puts "Sorted List is Now"
dict for {theKey theValue} $sortedDictGatesFinal {
    puts "$theKey -> $theValue"
}

set dictWithXors [determineXORs $sortedDictGatesFinal]
puts "Sorted List is Now with XORs"
dict for {theKey theValue} $dictWithXors {
    puts "$theKey -> $theValue"
}

set associatedPairList [determineAsscoiatedSumAndCarryGates $dictWithXors]
puts "Associated XOR and AND gates are:"
puts $associatedPairList

#vanishing polynomial list
set vanishingPolynomialList {}

puts "Creating Polynomials: "
set relabeledXORDict [renameDictToVariables $dictWithXors $associatedPairList $inputSize $outputList vanishingPolynomialList]
puts "\nInitial XOR Dict is: "
dict for {theKey theValue} $dictWithXors {
    puts "$theKey -> $theValue"
}

puts "\nNew Vanishing Polynomial list is: "
puts $vanishingPolynomialList


puts "\nRelabeled XOR dict is: "
dict for {theKey theValue} $relabeledXORDict {
    puts "$theKey -> $theValue"
}

puts "\nPolynomial List is: "
set polynomials [writePolynomials $relabeledXORDict $vanishingPolynomialList]
foreach item $polynomials {
    puts "$item"
}

puts "\n"


set fp [open "mult.sing" w+]

#now need to create ring for Singular
set ring [createRingForSingular $fp $relabeledXORDict $polynomials $inputList $outputList]
